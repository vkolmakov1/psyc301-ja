class IndeedJobBoard():
    ''' Helper class for a job_board_scraper script
        contains anything specific to indeed job board
    '''
    def __init__(self):
        self.BASE_URL = 'http://www.indeed.com'

        self.tags_to_keep_init = ['a.turnstileLink']

    def generate_url_extension(self, args):
        ''' Takes in args dictionary and returns a url extension'''
        return '/jobs?q={}&l={}%2c+{}&start={}'.format(args['query'], args['city'], args['state'], int(args['start']))

    def initialize_data(self, data, soup):
        ''' Takes in reference to data dictionary and
            reference to a list of bs tags and records
            urls and job titles to the data dictionary
        '''
        for tag in soup:
            if 'clk' in tag.get('href'):
                job_posting = {
                    'url': tag.get('href'),
                    'title': tag.get('title')
                }
                data.append(job_posting)
        return data
