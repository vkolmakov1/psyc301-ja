from indeed import IndeedJobBoard
from bs4 import BeautifulSoup
import requests
import re
from nltk.corpus import stopwords
import csv
import os.path
from random import randint
from time import sleep

def init_data(filename):
    ''' Takes in a csv file name and return a list with data'''
    data = []
    input_file = csv.DictReader(open(filename))
    for row in input_file:
        data.append(row)
    return data

def make_soup(text, tags_to_keep):
    ''' Returns a list of bs tags given text of the page
        and a list of tags (css selectors) to be kept'''
    soup = BeautifulSoup(text, 'html.parser')
    return soup if len(tags_to_keep) == 0 else soup.select(' '.join(tags_to_keep))


def get_page_content(url):
    ''' Returns page content given url '''
    res = requests.get(url)
    sleep(randint(1, 5))
    res.raise_for_status()
    return res.text


def clean_up_data(url):
    ''' Returns set of unique words from the webpage'''
    try:
        soup = make_soup(get_page_content(url), [])
    except Exception:
        return ''

    [tag.decompose() for tag in soup.find_all(['script', 'style', 'img'])]
    chunks = []
    for tag in soup.find_all(recursive=True):
        try:
            chunks.append(tag.get_text()
                          .decode('unicode_escape')
                          .encode('ascii', 'ignore')
                          .lower())
        except Exception:
            pass

    toks = re.split('\s', ' '.join(chunks))

    words = ' '.join(set([re.sub('[^a-zA-Z3+]', '', tok) for tok in toks]))
    swords = stopwords.words('english')

    return ' '.join([word for word in words.split(' ') if word not in swords and len(word) < 20])


def serialize_data(soup, JobBoard):
    ''' Given a list of bs tags return serialized data '''
    data = []

    # Fill up list with dictionaries that hold job titles and urls
    data = JobBoard.initialize_data(data, soup)

    print 'Found {} jobs!'.format(len(data))

    # After every job on a page is collected, follow through every url and
    # make a soup + pass it to the JobBoard class to handle
    for job_posting in data:
        url = JobBoard.BASE_URL + job_posting['url']
        job_posting['title'] = ' '.join(re.sub('[^a-zA-Z]', ' ', job_posting['title'])
                                        .split()).lower()
        print ' - Fetching {}...'.format(job_posting['title'])
        job_posting['keywords'] = clean_up_data(url)

    return remove_duplicates(data, 'url')


def remove_duplicates(data, prop):
    ''' Removes duplicates from list of dictionaries based on prop '''
    return [v for k, v in {d[prop]: d for d in data}.iteritems()]


def write_data(data, filename):
    file_exists = os.path.isfile(filename)

    with open(filename, 'a') as csv_file:
        fieldnames = ['url', 'title', 'keywords']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        if not file_exists:
            writer.writeheader()
        for item in data:
            writer.writerow({
                'url': item['url'],
                'title': item['title'],
                'keywords': item['keywords']
            })
    print 'Done writing {} jobs to {}!'.format(len(data), filename)
    csv_file.close()


def clean_up_recorded_data(filename):
    ''' Given filename with duplicates remove them '''
    write_data(remove_duplicates(init_data(filename), 'url'),
               'data/{}_clean.csv'.format(filename.split('.')[0]))


cities = [
    {'city': 'los+angeles', 'state': 'CA', 'max': 500},
    {'city': 'Philadelphia', 'state': 'PA', 'max': 490},
    {'city': 'Detroit,', 'state': 'MI', 'max': 160},
    {'city': 'Denver', 'state': 'CO', 'max': 270},
    {'city': 'Atlanta', 'state': 'GA', 'max': 270},
]

def start_scraping(search_params):
    JobBoard = IndeedJobBoard()
    base_url = JobBoard.BASE_URL
    tags_to_keep = JobBoard.tags_to_keep_init
    filename = '{}_data.csv'.format(search_params['city'].lower())

    search_params['start'] = 0
    search_params['query'] = 'data+scientist'

    while(search_params['start'] < search_params['max']):
        print 'Doing start = {}'.format(search_params['start'])
        url_extension = JobBoard.generate_url_extension(search_params)
        url = base_url + url_extension
        try:
            write_data(serialize_data(make_soup(get_page_content(url), tags_to_keep), JobBoard),
                       filename)
        except Exception:
            print 'Couldn\'t get the job list'

        search_params['start'] += 10

    clean_up_recorded_data(filename)
