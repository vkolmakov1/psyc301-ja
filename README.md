# Psych 301 job analysis project

## General info

Data is stored in data_clean.csv, keywords contain a set of unique words that I got from going through every job posting for 'Data Scientist' in Chicago, IL, on indeed and removing duplicates.

[PIL](http://www.pythonware.com/products/pil/) is required for [word\_cloud](https://github.com/amueller/word_cloud)

## Instructions

* I'm using python 2.7, although 3 would work just as good

* Example:

```bash
$ python
```
```python
 >>> from data_processing.process_data import *
 >>> phrases = ['java', 'python']
 >>> freq_dist(init_data('data_processing/data_clean.csv'), phrases)
 {'python': 110, 'java': 35}
 >>> phrases.append('javascript')
 >>> freq_dist(init_data('data_processing/data_clean.csv'), phrases)
 {'python': 110, 'javascript': 33, 'java': 35}
```

## TODO:

* Come up with a list of words for interesting skills: technical/personality_traits etc
* Do some ~~cool~~ ~~interesting~~ ~~useful~~ data visualization.
* Collect data for more cities / from more job boards?
