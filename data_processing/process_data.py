import csv
import os
import json


def init_data(filename):
    ''' Takes in a csv file name and return a list with data'''
    data = []
    input_file = csv.DictReader(open(filename))
    for row in input_file:
        data.append(row)
    return data


def freq_dist(data, categories, name, key='keywords'):
    ''' Returns a tuple that contains name of a distribution in 0th position
        and a list of frequency tuples in second'''
    distr = {}
    for category in categories:
        for data_point in data:
            if any(all(word in data_point[key].split() for word in phrase.lower().split()) for phrase in category[1]):
                distr[category[0]] = 1 if category[0] not in distr else distr[category[0]] + 1
    return (name, [(k, v) for k, v in distr.items()])


def fd_to_list(freq_dist, keys=('name', 'size')):
    ''' Given a list of tuples with frequencies and a tuple with keys
        for json object produces a list  for d3js visualisation '''
    return [{keys[0]: e[0], keys[1]: e[1]} for e in freq_dist]


def get_d3_json(freq_distrs, keys=('name', 'children')):
    ''' freq_distrs is a list with dictionaries, each has name and data
        (actual frequency distribution) fields'''
    return json.dumps({
        'name': '!root',
        'children': [{keys[0]: fd[0], keys[1]: fd_to_list(fd[1])} for fd in freq_distrs]
    }, indent=4, separators=(',', ': '))


def write_file(filename, data):
    print("writing file {}".format(filename))
    f = open(filename, 'w')
    f.write(data)
    f.close()
    return f.closed


def make_d3_histogram_json(keywords, name, path_to_data):
    data_files = os.listdir('data')
    data = []
    for f in data_files:
        old_len = len(data)
        data += init_data(os.path.join('data', f)) if f.endswith('.csv') else []
    print('Done writing histogram with {} datapoints!'.format(len(data)))
    return write_file(os.path.join(path_to_data, '{}.json'.format(name)),
                      get_d3_json([freq_dist(data, k, k[0]) for k in keywords]))


def run():
    KWDS = [('direction', [direction]),
            ('tools', [tools]),
            ('languages', [programming_languages]),
            ('personality', [personality])]

    PRSNKWDS = [
        ('openness', [openness]),
        ('extraversion', [extraversion]),
        ('neuroticism', [neuroticism]),
        ('agreebleness', [agreebleness]),
        ('conscientiosness', [conscientiosness]),
    ]
    iterable = KWDS
    for kwds in iterable:
        print('doing', kwds[0])
        make_d3_histogram_json(kwds[1], kwds[0], '../psyc301presentation/data')

direction = [
    ('NLP', ('Text Mining', 'NLP', 'Natural language',)),
    ('Bioinformatics', ('bioinformatics', 'biology informatics', 'bio informatics biology')),
    ('Machine Learning', ('Machine Learning', 'ml',)),
    ('Data Visualization', ('Data visualization', 'data exploration', 'charts',)),
    ('Pure Math', ('Math', 'stat',)),
    ('Big Data', ('big data', 'map reduce', 'hadoop',)),
    ('Algorithm Design', ('algorithm design', 'algorithms design',)),
    ('Analytics', ('business analyst', 'analytics',)),
    ('Data Mining', ('data mining',)),
]

tools = [
    ('Excel', ('Microsoft Excel', 'excel', 'toolpak',)),
    ('RHIPE', ('RHIPE',)),
    ('NTLK', ('NTLK', 'natural language',)),
    ('Spark', ('Spark', 'apache spark',)),
    ('Storm', ('Storm',)),
    ('d3', ('d3js', 'd3',)),
    ('Cassandra', ('cassandra',)),
    ('SQL', ('mysql', 'postgresql', 'sql',)),
    ('NoSQL', ('cassandra', 'mongodb', 'nosql',)),
    ('tableau', ('tableau',)),
    ('Mathlab', ('mathlab', 'math lab')),
    ('SciPy', ('scipy', 'scikit learn', 'scikit'))
]

programming_languages = [
    ('Java', ('Java',)),
    ('Python', ('python', 'python3', 'py',)),
    ('JavaScript', ('javascript', 'nodejs',)),
    ('R', ('r', 'rstudio', 'r studio',)),
    ('Hadoop', ('hadoop',)),
    ('Ruby', ('ruby', 'ruby rails',)),
    ('SAS', ('sas',)),
    ('C++', ('c++',)),
    ('C#', ('c',)),  # yeah, gross overgeneralization, but who uses c for data analysis anyway
    ('Scala', ('scala',)),
    ('Haskell', ('haskell',)),
    ('Lisp&Clojure', ('Lisp', 'Clojure')),
]

personality = [
    ('Extraversion', ('extraversion', 'extravert', 'extraverted', 'outgoing', 'talkative', 'sociable', 'social', 'leader')),
    ('Neuroticism', ('obsessive', 'obsess', 'nervous', 'nerve', 'short temper')),
    ('Agreebleness', ('pleasant', 'agreeble', 'nice', 'outgoing', 'sympathetic', 'cooperative')),
    ('Conscientiousness', ('on time', 'timely manner', 'time management', 'good scheduling', 'schedule')),
    ('Openness to experience', ('open to new experience', 'curious', 'curiousity', 'learner fast', 'easy learn', 'new techologies learn')),
]

extraversion = [
    ('extraversion', ('extraversion',)),
    ('extravert', ('extravert',)),
    ('extraverted', ('extraverted',)),
    ('outgoing', ('outgoing',)),
    ('talkative', ('talkative',)),
    ('sociable', ('sociable',)),
    ('social', ('social',)),
    ('leader', ('leader',)),
]

neuroticism = [
    ('obsessive', ('obsessive',)),
    ('obsess', ('obsess',)),
    ('nervous', ('nervous',)),
    ('nerve', ('nerve',)),
    ('short temper', ('short temper',)),
]

agreebleness = [
    ('pleasant', ('pleasant',)),
    ('agreeble', ('agreeble',)),
    ('nice', ('nice',)),
    ('outgoing', ('outgoing',)),
]

conscientiosness = [
    ('on time', ('on time',)),
    ('timely manner', ('timely manner',)),
    ('time management', ('time management',)),
    ('good scheduling', ('good scheduling',)),
    ('schedule', ('schedule',)),
]

openness = [
    ('open to new experience', ('open to new experience',)),
    ('curious', ('curious',)),
    ('curiousity', ('curiousity',)),
    ('learner fast', ('learner fast',)),
    ('easy learn', ('easy learn',)),
    ('new techologies learn', ('new techologies learn',)),
]
