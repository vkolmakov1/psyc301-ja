import d3 from 'd3';

function makeWordCloud(name, filename, containerId) {
    const color = d3.scale.category10();

    const width = 800;
    const height = 600;

    function drawCloud(words) {
        const svg = d3.select(`#${containerId}`)
                  .attr('width', width + 50)
                  .attr('height', height + 50)
                  .append('g')
                  .attr('transform', `translate('+${width / 2}', '+${height / 2}')`);

        const maxFrequency = d3.max(words, d => d.frequency);
        const scale = d3.scale.linear()
                  .domain([0, maxFrequency])
                  .range([10, 100]);

        this[name].animateCloud = () => {
            const vis = svg.selectAll('text')
                    .remove()
                    .data(words);

            vis.enter()
                .append('text')
                .style('font-size', 1)
		.style('fill', (d, i) => color(i))
		.attr('text-anchor', 'middle')
		.text((d) => d.text);

            vis.transition()
                .duration(600)
                .style('font-size', (d) => `${+d.frequency}px`)
		.attr('transform', (d) => `translate(${[d.x, d.y]})rotate(${d.rotate})`);
        };
    }

    d3.json(filename, (json) => {
        const data = json.children[0].children;
        const maxFrequency = d3.max(data, d => d.size);
        const scale = d3.scale.linear()
                  .domain([0, maxFrequency])
                  .range([30, 150]);

        const layout = d3.layout.cloud()
                  .size([width, height])
                  .padding(10)
                  .font('impact')
                  .fontSize((d) => +d.frequency)
                  .words(data.map((d) => {
                      return { text: d.name, frequency: +scale(d.size) };
                  }))
                  .on('end', drawCloud);
        layout.start();
    });
}

const wordCloudUtil = {};
wordCloudUtil.makeWordCloud = makeWordCloud.bind(wordCloudUtil);
module.exports(wordCloudUtil);
