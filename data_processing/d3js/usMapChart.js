import d3 from 'd3';
import topojson from 'topojson';

function makeUsMap(containerId) {
    const width = 960;
    const height = 500;

    const path = d3.geo.path();
    const force = d3.layout.force().size([width, height]);

    const svg = d3.select(`#${containerId}`)
            .attr('width', width)
            .attr('height', height);

    d3.json('data/chicago.json', (error, us) => {
        if (error) {
            throw error;
        }

        const states = topojson.feature(us, us.data);
        const nodes = [];
        const links = [];

        states.features.forEach((d) => {
            if (d.id === 2 || d.id === 15 || d.id === 72) {
                return; // lower 48
            }

            let centroid = path.centroid(d);

            if (centroid.some(isNaN)) {
                return;
            }

            centroid.x = centroid[0];
            centroid.y = centroid[1];
            centroid.feature = d;
            nodes.push(centroid);
        });

        d3.geom.voronoi().links(nodes).forEach((link) => {
            let dx = link.source.x - link.target.x;
            let dy = link.source.y - link.target.y;
            link.distance = Math.sqrt(dx * dx + dy * dy);
            links.push(link);
        });

        force.gravity(0)
            .nodes(nodes)
            .links(links)
            .linkDistance(d => d.distance)
            .start();

        let link = svg.selectAll('line')
                .data(links)
                .enter()
                .append('line')
                .attr('x1', (d) => d.source.x)
                .attr('y1', (d) => d.source.y)
                .attr('x2', (d) => d.target.x)
                .attr('y2', (d) => d.target.y);

        // states to be marked
        const markStates = {
            45: 295,
            21: 1120 + 548,
            16: 1100,
            17: 559,
            27: 300,
            6: 178,
            41: 226,
            48: 164,
            7: 58,
            12: 1124,
            18: 548,
            36: 122,
            8: 122,
            0: 977,
            35: 1095,
        };

        let node = svg.selectAll('g')
                .data(nodes)
                .enter()
                .append('g')
                .attr('transform', (d) => `translate(${-d.x},${-d.y})`)
                .call(force.drag)
                .append('path')
                .attr('transform', (d) => `translate(${d.x},${d.y})`)
                .attr('d', (d) => path(d.feature))
                .style('fill', (d) => {
                    // quick and dirty
                    if (markStates[d.index]) {
                        return markStates[d.index] > 500 ? '#ce5858' : '#dd8888';
                    } else {
                        return '#d2d2d2';
                    }
                });

        force.on('tick', (e) => {
            link.attr('x1', d => d.source.x)
                .attr('y1', d => d.source.y)
                .attr('x2', d => d.target.x)
                .attr('y2', d => d.target.y);

            node.attr('transform', d => `translate(${d.x},${d.y})`);
        });
    });
}

const usMapUtil = {};
usMapUtil.makeUsMap = makeUsMap.bind(usMapUtil);
module.exports(usMapUtil);
