import d3 from 'd3';

function makeHistogram(name, filename, containerId) {
    const margin = { top: 40, right: 20, bottom: 80, left: 40 };
    const width = 960 - margin.left - margin.right;
    const height = 500 - margin.top - margin.bottom;

    const svg = d3.select(`#${containerId}`)
              .attr('width', width + margin.left + margin.right)
              .attr('height', height + margin.top + margin.bottom);

    let x = d3.scale.ordinal()
            .rangeRoundBands([0, width], 0.15);

    let y = d3.scale.linear()
            .range([height, 0]);

    d3.json(filename, (jsonData) => {
        const data = jsonData.children[0].children;
        const maxSize = d3.max(data, (d) => d.size);

        x.domain(d3.range(data.length));
        y.domain([0, maxSize]);

        // creating g element, so we can refer to our group later
        let bars = svg.selectAll('g')
                .append('g')
                .data(data, d => d.name)
                .enter();

        bars.append('rect')
            .attr('x', (d, i) => x(i))
            .attr('y', (d) => y(d.size) + margin.top)
            .attr('width', x.rangeBand())
            .attr('height', (d) => height - y(d.size))
            .attr('fill', (d) => d.size === maxSize ? '#ce5858' : '#b2b2b2');

        bars.append('text')
            .text((d) => d.size)
            .attr('class', 'frequency')
            .attr('x', (d, i) => x(i))
            .attr('y', (d) => y(d.size) + margin.top - 4)
            .style('font-size', '20px');

        bars.append('text')
            .text(d => d.name)
            .attr('class', 'name')
            .attr('x', (d, i) => x(i))
            .attr('y', height + margin.top + 20)
            .attr('transform', (d, i) => `rotate(15 ${x(i)} ${height + margin.top + 20})`)
            .style('font-size', '16px');

        this[name].sortBars = (key) => {
            // just move every elemetn
            ['rect', '.frequency', '.name'].forEach((elem) => {
                svg.selectAll(elem)
                    .sort((a, b) => d3.ascending(a[key], b[key]))
                    .transition()
                    .delay((d, i) => key === 'size' ? i * 150 : 0)
                    .duration(key === 'size' ? 750 : 0)
                    .attr('x', (d, i) => x(i))
                    .attr(elem === '.name' ? 'transform' : '__dummy__', (d, i) => `rotate(15 ${x(i)} ${height + margin.top + 20})`); // for rotated text,super ugly:(
            });
        };
    });
}

const histogramUtil = {};
histogramUtil.makeHistogram = makeHistogram.bind(histogramUtil);
module.exports(histogramUtil);
